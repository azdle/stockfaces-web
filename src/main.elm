module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (id ,class, href, target, src, alt, style, placeholder, readonly)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (string, array, Decoder)
import Json.Decode.Pipeline exposing (decode, required, hardcoded)
import Date exposing (Date)
import Date.Distance as Distance
import Time exposing (Time, second)
import Task exposing (perform)
import Array exposing (Array, empty, append)
import Array.Extra

main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- CONSTS
perPage = 25


-- MODEL

type alias Model =
  { time: Time
  , faces: Faces
  , pages: Int
  , tags: List String }


init : (Model, Cmd Msg)
init =
  (!) ( Model 0 empty 0 [] )
    [ getFaces perPage 0
    , perform Tick Time.now
    ]


-- UPDATE

type Msg
  = ReqNewFaces
  | NewFaces (Result Http.Error Faces)
  | SelectToggleFace Int
  | CopyFaceLinks
  | Tick Time


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Tick newTime ->
      ({model | time = newTime}, Cmd.none)

    ReqNewFaces ->
      (model, getFaces perPage model.pages)

    NewFaces (Ok newFaces) ->
      let
        newModel =
          { model | faces =  append model.faces newFaces }
        newModel2 =
          { newModel | pages = newModel.pages + 1 }
      in
        (newModel2, Cmd.none)

    SelectToggleFace idx ->
      let
        toggleFaceSelected : Face -> Face
        toggleFaceSelected face = { face | selected = not face.selected }
      in
        ( { model | faces = Array.Extra.update idx toggleFaceSelected model.faces }
        , Cmd.none
        )

    CopyFaceLinks ->
      (model, Cmd.none)

    NewFaces (Err _) ->
      (model, Cmd.none)


-- VIEW

view : Model -> Html Msg
view model =
  div []
    [ ul []
      ( (List.map (faceItem model.time) (Array.toIndexedList model.faces))
        ++ [article [class "media"]
            [ a [onClick ReqNewFaces, class "button is-primary"]
              [ text "Load More"]
            ]
           ]
      )
    , textarea [ class "textarea"
               , id "facelinks"
               , placeholder "Select some faces"
               , readonly True
               ]
      [ text
      ( String.join "\n"
        (  Array.toList
          ( Array.map getFaceUrl
            (Array.filter faceIsSelected model.faces
            )
          )
        )
      ) ]
    ]


faceItem : Time -> (Int, Face) -> Html Msg
faceItem now (idx, face) =
  let
    origDate =
      Date.fromString face.created

    reltime =
      case origDate of
        Ok date ->
          Distance.inWords date (Date.fromTime now)
        Err _ ->
          ""
    selClassStr =
      case face.selected of
        True ->
          " selected"
        False ->
          ""
    selIcon =
      case face.selected of
        True ->
          "✓"
        False ->
          ""

  in
    article [ class "media" ]
      [ div [ class "media-left" ]
        [ figure [ class ("image is-64x64" ++ selClassStr) ]
          [ div
            [ class "selected-icon"
            , onClick (SelectToggleFace idx)
            ]
            [ text selIcon
            ]
          , img [src (getFaceUrl face) ] []
          ]
        ]
      , div [ class "media-content" ]
        [ div [ class "content" ]
          [ p []
            [ text "Artist: "
            , a [ href ("http://unsplash.com/@" ++ face.artistusername)
                , target "_blank" ]
              [ strong []
                [ text face.artistname
                ]
              , text " "
              , small []
                [ text ("@" ++ face.artistusername)
                ]
              ]
            , small [ style [("float", "right")] ]
              [ text reltime
              ]
            , br [] []
            , text "Via "
            , a [ href "https://unsplash.com"
                , target "_blank" ]
              [ text "Unsplash"]
            ]
          ]
          , nav [ class "level" ]
          [ div [ class "level-left" ]
            [ a [ class "level-item"
                , href  ("https://i.stockfaces.com/" ++ face.id)
                , target "_blank" ]
              [ span [ class "icon is-small" ]
                [ i [ class "fa fa-link" ] []
                ]
              ]
            ,  a [ class "level-item"
                 , href ("https://unsplash.com/photos/" ++ face.srcid)
                 , target "_blank" ]
              [ span [ class "icon is-small" ]
                [ i [ class "fa fa-external-link" ] []
                ]
              ]
            ]
          ]
        ]

      ]


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every (30 * second) Tick


-- FACES

getFaces : Int -> Int -> Cmd Msg
getFaces count page =
  let
    req_url =
      "https://api.stockfaces.com/v1/images?count="
      ++ (toString count)
      ++ "&page="
      ++ (toString page)
  in
    Http.send NewFaces (Http.get req_url facesDecoder)


type alias Faces =
  Array Face

facesDecoder : Decoder Faces
facesDecoder =
  Decode.array faceDecoder

type alias Face =
  { id: String
  , srcid: String
  , artistname: String
  , artistusername: String
  , created: String
  , selected: Bool
  }

faceIsSelected : Face -> Bool
faceIsSelected face = face.selected

getFaceUrl : Face -> String
getFaceUrl face = "https://i.stockfaces.com/" ++ face.id

faceDecoder : Decoder Face
faceDecoder =
  decode Face
    |> required "id" string
    |> required "srcid" string
    |> required "artistname" string
    |> required "artistusername" string
    |> required "created" string
    |> hardcoded False
